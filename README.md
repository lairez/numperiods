
# numperiods

## How to install

``` shell
sage -pip install --user git+https://github.com/mkauers/ore_algebra.git --upgrade
sage -pip install --user lmdb
sage -pip install --user pytest
```

Optionally, you can run the tests
``` shell
sage -python -m pytest
```

## How to build a database of quartic surfaces

```shell
# Put Fermat in the database
sage -python -m quarticdb --verbose seed

# Crunch periods
# This can be run multiple times simultaneously
sage -python -m quarticdb periods

# Snapshot database
sage -python -m lmdb copy -e database snapshot

# Crunch Picard lattices
# This can be run multiple times simultaneously
# Do it from time to time when you have new periods
sage -python -m quarticdb picard

# New option available after having computed some Picard lattices
sage -python -m quarticdb periods --only-pic 13

# If you have a data.mdb around coming from another person/computer, you can
# merge in the current database with the following command
# It never overwrites.
sage -python -m quarticdb merge-in path/to/data.mdb
```

