from sage.matrix.constructor import Matrix
from sage.misc.cachefunc import cached_method
from sage.misc.misc_c import prod
from sage.modules.free_module_element import vector
from sage.rings.integer_ring import ZZ
from sage.rings.rational_field import QQ

import logging

logger = logging.getLogger(__name__)

class Homology(object):
    _instances = {}

    @staticmethod
    def get_instance(ring, degree):
        if not (ring, degree) in Homology._instances:
            Homology._instances[ring, degree] = Homology(ring, degree)
        return Homology._instances[ring, degree]

    def __init__(self, ring, degree):
        self.ring = ring
        self.degree = degree
        self.dim = self.ring.ngens()-2
        logger.debug("Constructing homology in dimension %d and degree %d." % (self.dim, self.degree))

        gens = self.ring.gens()
        self._prim_ideal = self.ring.ideal([sum(v**i for i in range(self.degree)) for v in gens] + [prod(gens)-1])

        # This is useful to take this particular basis to compare with previous computations
        #w,x,y,z = gens
        #self.prim_basis = [parent(1), y, x, w, y**2, x*y, w*y, x**2, w*x, w**2, x*y**2, w*y**2, x**2*y, w*x*y, w**2*y, w*x**2, w**2*x, x**2*y**2, w*x*y**2, w**2*y**2, w*x**2*y]
        self._prim_basis = sorted(self._prim_ideal.normal_basis())

    def prim_basis(self):
        return self._prim_basis

    # Implements Looijenga's computation of Pham's cycle intersection
    # Is this implementation false ?
    def _elementary_intersection__(self, a1, a2):
        plus = 0
        minus = 0
        match = 0
        for x in zip(a1,a2):
            rem = ZZ(x[0]-x[1]).mod(self.degree)
            if rem == 1:
                plus += 1
            elif rem + 1 == self.degree:
                minus += 1
            elif rem == 0:
                match += 1

        sign = (-1)**((self.dim*(self.dim+1))//2)
        if match == len(a1):
            return sign*(1+(-1)**self.dim)
        elif match + plus == len(a1):
            return sign*(-1)**plus
        elif match + minus == len(a1):
            return sign*(-1)**minus
        else:
            return 0

    # This is another formula for the same thing.
    def _elementary_intersection(self, a1, a2):
        assert len(a1) == self.dim+2 and len(a2) == len(a1)
        beta = [a1[i]-a2[i]-a1[self.dim+1]+a2[self.dim+1] for i in range(len(a1)-1)]
        sign = (-1)**((self.dim*(self.dim+1))//2)

        def chi(b):
            if ZZ(b).mod(self.degree) == 0:
                return 1
            elif ZZ(b).mod(self.degree) == 1:
                return -1
            else:
                return 0

        return sign*(prod(chi(b) for b in beta) - prod(chi(b+1) for b in beta))

    @cached_method
    def prim_intersection_matrix(self):
        B = [tuple(m.exponents()[0]) for m in self._prim_basis]
        return Matrix([[self._elementary_intersection(a,b) for a in B] for b in B])

    # Helper function for the next one.
    def _tau(self, x):
        rem = ZZ(x).mod(2*self.degree)
        if rem == 1:
            return 1
        elif rem + 1 == 2*self.degree:
            return -1
        else:
            return 0

    # Return < L, cycle corresponding to a > for a in ph_basis
    def _intersections_with_linear_space(self):
        res = []
        for b in self._prim_basis:
            a = b.exponents()[0]
            res.append(self._tau(2*a[self.dim] - 2*a[self.dim+1] - 1) *
                       prod(self._tau(2*a[2*i] - 2*a[2*i+1] + 1) for i in range(self.dim//2)))
        return vector(res)

    def _prim_part_of_extra_cycle(self):
        return self.prim_intersection_matrix().solve_right(self._intersections_with_linear_space())

    @cached_method
    def intersection_matrix(self):
        if self.dim % 2 == 0:
            r = self._prim_part_of_extra_cycle()
            i = self._intersections_with_linear_space()
            selfinter = 1/QQ(self.degree) + r*i
            assert selfinter.is_integer()

            return Matrix.block([[self.prim_intersection_matrix(), (-1)**(self.dim)*i.column()], [i.row(), selfinter]])
        else:
            return self.prim_intersection_matrix()

    @cached_method
    def polarization(self):
        assert self.dim % 2 == 0
        return vector(list(-self.degree*self._prim_part_of_extra_cycle()) + [self.degree])


