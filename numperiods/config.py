

precision = 2000

nice_paths = True

unsafe_cyclic_decomposition = True

fail_fast = False
time_to_compute_picard_fuchs_equations = 3*60
time_to_compute_numerical_transition_matrices = 30*60
time_to_count_smooth_rational_curves = 2*60
max_dimension_of_a_cyclic_space = 11



