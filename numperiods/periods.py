# -*- coding: utf-8 -*-

import logging

from sage.arith.misc import random_prime
from sage.functions.other import factorial
from sage.matrix.constructor import Matrix
from sage.misc.cachefunc import cached_method
from sage.misc.misc_c import prod
from sage.modules.free_module_element import vector
from sage.rings.all import *
from sage.rings.complex_arb import ComplexBallField
from sage.rings.complex_interval_field import ComplexIntervalField
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.real_double import RDF
from sage.rings.rational_field import Q

from . import cohomology
from . import homology
from . import hypersurface
from . import family
from . import config
from . import integer_relations
from . import picard
from . import transcendental

logger = logging.getLogger(__name__)

def symtuple(self, t):
    return tuple(sum(x^i for x in t) for i in range(len(t)))



def _make_SimplePeriods_v0(pol, matrix, info):
    CBF = ComplexBallField(matrix.base_ring().precision()-10)
    return SimplePeriods(pol, matrix.change_ring(CBF), info)


def _make_SimplePeriods_v1(pol, matrix):
    CBF = ComplexBallField(matrix.base_ring().precision()-10)
    return SimplePeriods(pol, matrix.change_ring(CBF))



class SimplePeriods(object):
    def __init__(self, pol, matrix, info={}):
        if isinstance(pol, hypersurface.Hypersurface):
            self.hypersurface = pol
        else:
            self.hypersurface = hypersurface.Hypersurface(pol)

        self.info = info
        self.matrix = matrix
        #self._augment_matrix_with_extra_cycle()
        assert matrix.nrows() == len(self.hypersurface.cohomology().holomorphic_forms())

    def __reduce__(self):
        CIF = ComplexIntervalField(self.matrix.base_ring().precision()+10)
        return _make_SimplePeriods_v1, (self.hypersurface.pol, self.matrix.change_ring(CIF))


    def augmented_matrix(self):
        prim_homology_dim = len(self.hypersurface.homology().prim_basis())
        if self.hypersurface.dim % 2 == 0:
            extra = self.matrix * self.hypersurface.homology()._prim_part_of_extra_cycle()
            return self.matrix.augment(extra)
        else:
            return self.matrix

    def _save_as(self):
        return {self.hypersurface.keyid() + '/': {
            'simple_periods@': self,
            'simple_periods.info': self.info
        }}

    def picard(self, check_lines=True, **LLLargs):
        periods = self.augmented_matrix().transpose()
        ir = integer_relations.IntegerRelations(periods, **LLLargs)

        # Compute an absolute "half certificate"
        imatflip = self._canonical_qf()
        sv = imatflip.singular_values()
        smax = sv[0]
        smin = sv[-1]
        hc = ir.half_certificate().copy()
        hc['logB'] = int(smin.sqrt().log(10).floor()) + hc['logB']
        hc['N'] = int((smax.sqrt()*hc['N']).ceil())

        pic = picard.PicardLattice(self.hypersurface, ir.basis, self.hypersurface.homology().intersection_matrix(), check_lines=check_lines, half_certificate=hc)
        pic.threshold = ir.threshold()
        return pic

    def _canonical_qf(self):
        # This is explained in Sec. 4.2 of the paper

        if self.hypersurface.degree != 4 or self.hypersurface.dim != 2:
            raise NotImplementedError()

        hom =  self.hypersurface.homology()
        imat = hom.intersection_matrix()
        plus_dual = Matrix([(c.real(), c.imag()) for c in self.augmented_matrix().list()])
        # We change ring to RDF (real double field), because kernel computation
        # is not implemented in interval arithmetic. That should not be a
        # problem because the rows of plus_dual are orthogonal, so the
        # numerical stability is good.
        plus_dual = plus_dual.augment(imat*hom.polarization()).change_ring(RDF)
        # The rows of plus_dual are a orthogonal basis, in cohomology, of the real trace
        # of H^{2,0}+H^{2,0} and the fundamental class of the hyperplane section.

        # The same thing in homology
        plus = plus_dual.transpose() * imat.inverse()
        minus = Matrix.identity(plus.ncols()) - plus_dual*plus_dual.pseudoinverse()
        # The line below behaves badly (numerically)
        # minus = plus_dual.left_kernel().matrix()

        # We now have a nice decomposition of the homology. The lines of minus
        # are not independent, but this improve numerical stability.
        hbasis = plus.stack(minus)
        hbasis = hbasis.pseudoinverse()

        # And we compute the intersection matrix in this basis, with a sign flip.
        imatcan = Matrix.block_diagonal([plus * imat * plus.transpose(), -minus * imat * minus.transpose()])
        # And we revert to the standard basis
        imatcan = hbasis * imatcan * hbasis.transpose()

        return imatcan

    def _partial_lattice(self, tvec, i, j, **LLLargs):
        mat = vector([tvec[j]*tvec[k] for k in range(tvec.length())]
                     + [-tvec[i]*tvec[k] for k in range(tvec.length())]).column()

        ir = integer_relations.IntegerRelations(mat, **LLLargs)
        basis = ir.basis[:,:tvec.length()]

        assert basis.rank() == basis.nrows()
        transfer = basis.pseudoinverse() * ir.basis[:,tvec.length():]

        return dict(basis=basis, transfer=transfer)

    def transcendental(self, **LLLargs):
        assert self.hypersurface.dim == 2 and self.hypersurface.degree == 4;

        pic = self.picard()
        tbasis = pic.basis.change_ring(ZZ).right_kernel().matrix().LLL()
        tvec = vector(self.augmented_matrix() * tbasis.pseudoinverse())

        partials = {}
        tlat = ZZ**tvec.length()
        sub = tlat
        for k in range(1, tvec.length()):
            partials[k] = self._partial_lattice(tvec, 0, k, **LLLargs)
            sub = sub.intersection(tlat.submodule(partials[k]['basis']))

            # Early abort
            if sub.rank() == 1:
                break

        if sub.rank() == 1:
            return transcendental.TranscendentalLattice(self.hypersurface, tbasis, Matrix.identity(tvec.length()))

        endobasis = []
        for b in sub.basis():
            endobasis.append(Matrix([b] + [b*partials[k]['transfer'] for k in range(1, tvec.length())]))

        dim = len(endobasis)
        i = -1
        gen = endobasis[-1]
        while gen.minimal_polynomial().degree() < dim:
            i = (i+1) % dim
            gen += endobasis[i]

        return transcendental.TranscendentalLattice(self.hypersurface, tbasis, gen)



def _make_PrimitivePeriods_v0(pol, matrix):
    CBF = ComplexBallField(matrix.base_ring().precision()-10)
    return PrimitivePeriods(pol, matrix.change_ring(CBF))


class PrimitivePeriods(object):
    def __init__(self, pol, matrix, info={}):
        if isinstance(pol, hypersurface.Hypersurface):
            self.hypersurface = pol
        else:
            self.hypersurface = hypersurface.Hypersurface(pol)

        self.matrix = matrix
        assert matrix.nrows() == len(self.hypersurface.cohomology().basis())
        assert matrix.ncols() == matrix.nrows()

        self.info = info

    def __reduce__(self):
        CIF = ComplexIntervalField(self.matrix.base_ring().precision()+10)
        return _make_PrimitivePeriods_v0, (self.hypersurface.pol, self.matrix.change_ring(CIF))


    def deformation(self, target, only_simple_periods=False, keep_family=False, path=None):
        lf = family.LinearFamily(start=self.hypersurface.pol, target=target, path=path)
        mat = lf.numerical_transition_matrix(only_holomorphic_forms=only_simple_periods)
        mat = mat * self.matrix

        info = dict(computation='deformation', reference=self.hypersurface.pol,
                    path=lf.path(only_simple_periods))
        if only_simple_periods:
            periods = SimplePeriods(target, mat, info)
        else:
            periods = PrimitivePeriods(target, mat, info)

        if keep_family:
            periods.family = lf

        return periods

    def simple_periods(self):
        holomorphic_forms = [self.hypersurface.cohomology().index_of_basis_elt(b)
                             for b in self.hypersurface.cohomology().holomorphic_forms()]
        mat = self.matrix.matrix_from_rows(holomorphic_forms)
        return SimplePeriods(self.hypersurface.pol, mat, dict(computation='from primitive periods'))

    def change_of_variables(self, chmat):
        from sage.symbolic.ring import SR
        from sage.symbolic.constants import I

        SRvars = tuple(SR.var(str(v), domain='real') for v in self.hypersurface.ring.gens())
        chtuple = tuple(chmat * vector(SRvars))
        chtuplei = tuple(chmat.inverse() * vector(SRvars))
        chpol = self.hypersurface.ring(self.hypersurface.pol(chtuplei).expand())

        content = chpol.content()
        new_hypersurface = hypersurface.Hypersurface(chpol/Q(content))

        det = chmat.determinant()
        chbasis = [ b(chtuple)*det * content**new_hypersurface.cohomology().weight(b) for b in new_hypersurface.cohomology().basis()]
        chbasis_real = [ self.hypersurface.ring(b.real()) for b in chbasis ]
        chbasis_imag = [ self.hypersurface.ring(b.imag()) for b in chbasis ]

        chmat_cohomology_real = Matrix([self.hypersurface.cohomology().coordinates(b) for b in chbasis_real])
        chmat_cohomology_imag = Matrix([self.hypersurface.cohomology().coordinates(b) for b in chbasis_imag])

        new_mat = (chmat_cohomology_real + I*chmat_cohomology_imag).change_ring(self.matrix.base_ring())*self.matrix
        return PrimitivePeriods(new_hypersurface, new_mat, info=
                                dict(computation='change of variables',
                                     mat=chmat,
                                     reference=self.hypersurface.pol))

    def picard(self, *args, **kwargs):
        return self.simple_periods().picard(*args, **kwargs)

    def _save_as(self):
        return {self.hypersurface.keyid() + '/': {
            'primitive_periods@': self,
            'primitive_periods.info': self.info
        }}


# Implements Theorem 4.17 in https://arxiv.org/pdf/1803.08068.pdf (Sertöz)
class FermatHypersurface(hypersurface.Hypersurface):
    def __init__(self, ring, degree, coeffs=None):
        gens = ring.gens()
        if coeffs is None:
            coeffs = {v: 1 for v in gens}

        pol = sum(coeffs[v]*v**degree for v in gens)
        super(FermatHypersurface, self).__init__(pol)

        self.fcoeffs = coeffs
        # This deals with Sertöz's (and others) sign convention
        self.fcoeffs[gens[-1]] = -self.fcoeffs[gens[-1]]

        self.CBF = ComplexBallField(config.precision)
        # This will be used by periods computation
        self._xi = self.CBF(QQ(2)/self.degree).exppii()

    @cached_method
    def _gamma(self, arg):
        return self.CBF(arg).gamma()

    @cached_method
    def _scaling(self, i):
        R = self.pol.parent()
        if self.fcoeffs[R.gen(i)] == 1:
            return 1
        if self.fcoeffs[R.gen(i)] == -1:
            return self._xi.rsqrt()

        return self.CBF(self.fcoeffs[R.gen(i)]).pow(-1/QQ(self.degree))

    def _elementary_integral(self, alpha, beta):
        assert len(alpha) == len(beta)
        assert len(alpha) == self.dim+2
        m = self.dim+1
        l = sum(alpha)
        assert l % self.degree == 0
        l = l//self.degree

        salpha = alpha[0:len(alpha)-1]
        last = QQ(alpha[len(alpha)-1])  # WARNING: In case alpha is an ETuple, alpha[-1] does not work as expected.
        f0 = prod(1-last/j/self.degree for j in range(1, l))
        f1 = prod(self._scaling(i)**alpha[i] for i in range(len(alpha)))
        f2 = prod((1-self._xi**(-a))/self.degree for a in salpha)
        f3 = self._xi**sum(alpha[i]*beta[i] for i in range(len(alpha)))
        f4 = prod(self._gamma(QQ(a)/self.degree) for a in salpha)/self._gamma(QQ(sum(salpha))/self.degree)

        # This accounts for the convention in family.py
        f5 = Integer(l-1).factorial()

        res = -f0*f1*f2*f3*f4*f5
        return res

    def _integral(self, dform, cycle):
        dform_dict = dform.dict()
        cycle_dict = cycle.dict()

        res = self.CBF.zero()
        for alpha0, u in dform_dict.items():
            alpha = tuple(a+1 for a in alpha0)
            for beta, v in cycle_dict.items():
                res += u*v*self._elementary_integral(alpha, beta)

        return res

    def primitive_periods(self):
        logger.info("Computing Fermat primitive periods.")
        mat = Matrix([
            [self._integral(df, cy) for cy in self.homology().prim_basis()]
            for df in self.cohomology().basis()])
        return PrimitivePeriods(self, mat, info=dict(computation='fermat'))

    def simple_periods(self):
        logger.info("Computing Fermat primitive periods.")
        mat = Matrix([
            [self._integral(df, cy) for cy in self.homology().prim_basis()]
            for df in self.cohomology().holomorphic_forms()])
        return SimplePeriods(self, mat, info=dict(computation='fermat'))




