
from sage.misc.cachefunc import cached_method
from .hypersurface import Hypersurface

def _make_TranscendentalLattice_v0(pol, basis, endoring_generator):
    return TranscendentalLattice(Hypersurface(pol), basis, endoring_generator)

class TranscendentalLattice(object):
    def __init__(self, hypersurface, basis, endoring_generator):
        self.hypersurface = hypersurface
        self.basis = basis
        self.endoring_generator = endoring_generator

    def __reduce__(self):
        return _make_TranscendentalLattice_v0, (self.hypersurface.pol, self.basis, self.endoring_generator)

    @cached_method
    def minimal_polynomial(self):
        return self.endoring_generator.minimal_polynomial()

    def endoring_dim(self):
        return self.minimal_polynomial().degree()

    def is_totally_real(self):
        return self.minimal_polynomial().number_of_real_roots() == self.endoring_dim()

    def _save_as(self):
        return {self.hypersurface.keyid() + '/':
                {'transcendental/' : {'lattice': self,
                                      'endoring_dim': self.endoring_dim(),
                                      'is_totally_real': self.is_totally_real()}}}

