import signal


from sage.arith.misc import gcd
from sage.arith.misc import random_prime
from sage.matrix.constructor import Matrix
from sage.misc.cachefunc import cached_method
from sage.modules.free_module_element import vector
from sage.quadratic_forms.quadratic_form import QuadraticForm
from sage.rings.complex_arb import ComplexBallField
from sage.rings.complex_interval_field import ComplexIntervalField_class
from sage.rings.finite_rings.finite_field_constructor import FiniteField
from sage.rings.integer_ring import ZZ
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.rational_field import QQ
from sage.rings.real_arb import RealBallField
from sage.rings.real_mpfi import RealIntervalField_class, RealIntervalField
from sage.rings.real_mpfr import RR

from . import config
from .hypersurface import Hypersurface

def _make_PicardLattice_v0(pol, basis, intersection_matrix):
    return PicardLattice(Hypersurface(pol), basis, intersection_matrix, check_lines=False)


def _make_PicardLattice_v1(pol, basis, intersection_matrix, half_certificate):
    return PicardLattice(Hypersurface(pol), basis, intersection_matrix,
                         check_lines=False, half_certificate=half_certificate)


class PicardLattice(object):
    def __init__(self, hypersurface, basis, intersection_matrix, check_lines=True, half_certificate=None):
        assert hypersurface.dim % 2 == 0

        self.ambient_intersection_matrix = intersection_matrix
        self.hypersurface = hypersurface
        self.basis = basis
        self.rank = basis.nrows()
        self.intersection_matrix = (self.basis * intersection_matrix * self.basis.transpose())

        # If this raises an error, then the polarization given in argument is wrong
        self.polarization = self.basis.solve_left(self.hypersurface.homology().polarization())

        # Sanity check
        assert ((self.polarization * self.intersection_matrix) * self.polarization
                == self.hypersurface.degree)

        # Basis of the orthogonal supplement of the polarization (useful for counting rational curves)
        self.prim_basis = (Matrix(self.polarization * self.intersection_matrix)
                           .change_ring(ZZ).right_kernel().matrix().LLL())

        self.prim_intersection_matrix = self.prim_basis * self.intersection_matrix * self.prim_basis.transpose()

        self._picard_lattice_of_surface = False
        if self.hypersurface.dim == 2:
            self._picard_lattice_of_surface = True
            self.module = ZZ**(self.rank)
            pic0 = self.module.submodule(self.prim_basis)
            picm = 4*self.module + self.module.submodule([self.polarization])
            inter = pic0.intersection(picm).matrix().LLL()
            self._quadratic_form = QuadraticForm( -inter * self.intersection_matrix * inter.transpose())
            self._positive_lattice = inter
            self._short_vectors = []

            #This is a really interesting check
            if check_lines:
                assert len(self.smooth_rational_curves(1)) == self.hypersurface.number_of_lines()

        self.half_certificate = half_certificate

    def __reduce__(self):
        return _make_PicardLattice_v1, (self.hypersurface.pol, self.basis,
                                        self.ambient_intersection_matrix, self.half_certificate)

    def _short_vectors_in_positive_lattice(self, length):
        if len(self._short_vectors) > length:
            return self._short_vectors[length]
        else:
            self._short_vectors = self._quadratic_form.short_vector_list_up_to_length(length+1)
            return self._short_vectors[-1]

    @cached_method
    def _divisors(self, genus, degree):
        assert self._picard_lattice_of_surface
        sv = self._short_vectors_in_positive_lattice(8*(2-2*genus)+2*degree*degree)
        if len(sv) > 0:
            sv = (Matrix(sv) * self._positive_lattice) + Matrix([degree for _ in sv]).transpose() * Matrix(self.polarization)
            return [t/4 for t in sv.rows() if gcd(t)%4==0]
        else:
            return []

    @cached_method
    def smooth_rational_curves(self, degree):
        div = self._divisors(0, degree)
        rc_lowerdeg = [c for d in range(1, degree) for c in self.smooth_rational_curves(d)]
        if len(rc_lowerdeg) == 0 or len(div) == 0:
            return div

        # Among all elements of div, we want to keep only those with non
        # negative intersection with the elements of rc_lowerdeg.
        rc_lowerdeg = Matrix(ZZ, rc_lowerdeg)
        prod = self.intersection_matrix.change_ring(ZZ)*rc_lowerdeg.transpose()

        ret = []
        blocksize = 100
        for k in range(0, len(div), blocksize):
            divm = Matrix(ZZ, div[k:k+blocksize])
            inter = divm * prod
            for r in range(inter.nrows()):
                if all(inter[r, c] >= 0 for c in range(inter.ncols())):
                    ret.append(div[k+r])

        return ret


    def precompute_smooth_rational_curves(self, maxdegree=20):
        if config.fail_fast:
            signal.alarm(config.time_to_count_smooth_rational_curves)
        for k in range(1, maxdegree+1):
            self.smooth_rational_curves(k)
        if config.fail_fast:
            signal.alarm(0)

    def _save_as(self):
        return {self.hypersurface.keyid() + '/': {'picard/' : {'lattice': self,
                                                               'rank': self.rank,
                                                               'half_certificate': self.half_certificate}}}



