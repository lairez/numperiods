
class FailFast(Exception):
    pass

from .cohomology import Cohomology, NotSmoothError
from .homology import Homology
from .periods import FermatHypersurface, SimplePeriods, PrimitivePeriods
from .integer_relations import IntegerRelations
from .hypersurface import Hypersurface
from .family import Family

from . import config
