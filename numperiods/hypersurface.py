
from sage.arith.misc import random_prime
from sage.matrix.constructor import Matrix
from sage.misc.cachefunc import cached_method
from sage.modules.free_module_element import vector
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.finite_rings.finite_field_constructor import FiniteField

from . import cohomology
from . import homology

class Hypersurface(object):
    def __init__(self, pol):
        #self.pol = pol/pol.content()
        self.pol = pol
        self.degree = pol.degree()
        self.dim = self.pol.parent().ngens()-2
        self.ring = self.pol.parent()


    def __reduce__(self):
        return self.pol.__reduce__()

    def homology(self):
        return homology.Homology.get_instance(self.pol.parent(), self.degree)

    @cached_method
    def cohomology(self):
        return cohomology.Cohomology(self.pol)

    def _minimizing_permutation(self):
        f = self.pol
        L = [(symtuple(t), t, f.coefficient(R.monomial(*t))) for t in f.exponents()]
        L.sort()
        M = Matrix([p[1] for p in L]).columns()
        M = [(symtuple(t), t, v) for t, v in zip(M, f.parent().gens())]
        M.sort()
        sol = {t[2]: v for t, v in zip(M, f.parent().gens())}
        return tuple(sol[v] for v in f.parent().gens())

    def keyid(self):
        import hashlib
        h = hashlib.sha256()
        h.update(self.pol._repr_())
        return h.hexdigest()

    def _save_as(self):
        return {self.keyid() + '/': {'polynomial': self.pol, 'polynomial.str': str(self.pol)}}

    def number_of_lines(self):
        """Return the number of lines embedded in the periods, with high
        probability. This is for crosschecking purpose.
        """

        K = FiniteField(random_prime(2**30, lbound=2**25))
        R = PolynomialRing(K, 'a', 2*self.dim)
        a = R.gens()
        Rt = PolynomialRing(R, 't')
        t = Rt.gen()

        line = vector([1, t] + [a[2*i] + t*a[2*i+1] for i in range(self.dim)]) * Matrix.random(K, self.dim+2)
        eqs = R.ideal(self.pol.change_ring(K)(tuple(line)).coefficients())
        if eqs.dimension() < 0:
            return 0
        else:
            assert eqs.dimension() == 0
            return len(eqs.normal_basis())

    def delsarte_picard_rank(self):
        from sage.arith.misc import gcd
        from sage.rings.integer_ring import ZZ
        from sage.functions.other import frac

        # Implements the computation of the Lefschetz number of 4-nomials, after Shioda.
        mat = Matrix(self.pol.exponents())

        try:
            assert mat.ncols() == 4
            #assert all(c == 1 for c in self.pol.coefficients())
            assert len(list(self.pol)) == 4
            assert mat.determinant() != 0
            assert self.pol.is_homogeneous()
            assert all(any(e == 0 for e in c) for c in mat.columns())
        except AssertionError:
            raise ArgumentError("This is not a Delsarte hypersurface.")


        delta = gcd(mat.adjoint().list())
        d = (mat.determinant()/delta).abs()

        # This computes I_2^d(0) \cap L_A
        ambient = ZZ**4
        M0 = (ambient.submodule(Matrix([(1,0,0,-1),(0,1,0,-1),(0,0,1,-1)])) + d*ambient)
        imB = (ambient.submodule(d*M0.matrix()*mat.inverse()) + d*ambient)
        LA = [p.lift() for p in list(imB / (d*ambient))]  # This is the list of all elements in L_A (equation 2.5)

        def condA(p):
            return all(e%d != 0 for e in p)

        def condB(p):
            return any(sum(frac(t*e/d) for e in p) != 2 for t in range(d) if gcd(t,d)==1)

        frakIlist = [p for p in LA if condA(p) and condB(p)]
        lefschetz = len(frakIlist)

        return (self.degree - 1)*(self.degree**2 -3*self.degree + 3) + 1 - lefschetz




