import pytest

from sage.matrix.constructor import Matrix
from sage.rings.integer_ring import ZZ
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.rational_field import QQ
from sage.symbolic.constants import I

from numperiods import periods

R = PolynomialRing(QQ, 'w,x,y,z')
w,x,y,z = R.gens()

def test_dimension_basically():
    f = periods.FermatHypersurface(R, 4)
    assert f.simple_periods().matrix.dimensions() == (1, 21)

def test_change_of_variable_basic():
    p1 = periods.FermatHypersurface(R, 4).primitive_periods()
    chmat = Matrix.circulant([0,1,0,0])
    p2 = p1.change_of_variables(chmat)

    # This checks that m is a GL(Z) matrix. (Otherwise change_ring raises an error.)
    m = (p1.matrix.inverse()*p2.matrix).change_ring(ZZ)
    assert m.determinant().abs() == 1

# This truly looks like an ultimate test: it checks that the computations of
# periods through Picard-Fuchs equations agrees with what we obtain with a
# change of variable.
@pytest.mark.parametrize("chmat", [
    Matrix(4, [1,1,0,0,1,-1,0,0,0,0,1,1,0,0,1,-1]),
    Matrix([[I**(i*j) for j in range(4)] for i in range(4)])])

def test_change_of_variable_ultimate(chmat):
    periods.config.precision = 200
    p0 = periods.FermatHypersurface(R, 4).primitive_periods()

    p1 = p0.change_of_variables(chmat)
    p2 = p0.deformation(p1.hypersurface.pol)
    p3 = p1.change_of_variables(chmat.inverse())

    assert (p0.matrix - p3.matrix).contains(p1.matrix.parent().zero())
    m = (p1.matrix.inverse()*p2.matrix).change_ring(ZZ)
    assert m.determinant().abs() == 1


