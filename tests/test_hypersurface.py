import pytest

from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.rational_field import QQ

from numperiods import hypersurface

def test_number_of_lines():
    R = PolynomialRing(QQ, 'w,x,y,z')
    w,x,y,z = R.gens()

    f = 71760*w**4 + 17940*w**3*x - 4784*w**2*x**2 + 340860*w*x**3 - 8970*x**4 - 22425*w**2*x*y - 161460*w*x**2*y - 8910*w*x*y**2 + 8970*x**2*y**2 - 8970*w*y**3 - 358800*x*y**3 - 13670280*w**3*z + 188370*w**2*x*z - 4485*w*x*y*z + 8970*y**3*z - 143520*w**2*z**2 - 17940*w*x*z**2 + 35880*w*y*z**2 - 17940*y**2*z**2 + 17940*x*z**3 + 35880*z**4
    assert hypersurface.Hypersurface(f).number_of_lines() == 0

    assert hypersurface.Hypersurface(x**4+y**4+z**4+w**4).number_of_lines() == 48

    f = x**4-x*y**3-z**4+z*w**3
    assert hypersurface.Hypersurface(f).number_of_lines() == 64

