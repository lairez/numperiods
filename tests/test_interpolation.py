
import pytest
from sage.rings.all import *
from numperiods.interpolation import *
from sage.matrix.constructor import Matrix
from ore_algebra.ore_algebra import DifferentialOperators


EI = EvaluationInterpolation

def test_polynomial_multi_evaluation_border_cases():
    for base_ring in [ZZ, QQ, GF(2), GF(4), GF(127)]:
        R = PolynomialRing(base_ring, 't')
        p = R.random_element(degree=100)
        assert EI(R, [0]).evaluate(p) == [p.constant_coefficient()]
        assert EI(R, []).evaluate(p) == []

def test_polynomial_multi_evaluation():
    for base_ring in [ZZ, QQ, GF(2), GF(4), GF(127)]:
        R = PolynomialRing(base_ring, 't')
        p = R.random_element(degree=100)
        points = [base_ring.random_element() for _ in range(30)]
        evs = EI(R, points).evaluate(p)
        for i in range(30):
            assert evs[i]==p(t=points[i])

def test_interpolation():
    for base_ring in [QQ, GF(50021)]:
        R = PolynomialRing(base_ring, 't')
        assert R.interpolation([(0,1),(0,2)]) == None
        assert R.interpolation([]) == 0
        assert R.interpolation([(base_ring.one(), base_ring.one())]) == R.one()
        points = list(set(base_ring.random_element() for _ in range(40)))
        pairs = [(p, base_ring.random_element()) for p in points]
        inter = R.interpolation(pairs)
        assert inter.degree() < len(pairs)
        for p, ev in pairs:
            assert inter(t = p) == ev

def test_rational_interpolation():
    for base_ring in [QQ, GF(2**31-1)]:
        R = PolynomialRing(base_ring, 't')
        assert R.rational_interpolation([(1,2),(1,0)]) == None
        assert R.rational_interpolation([]) == (0,1)
        #assert R.rational_interpolation([(base_ring.one(), base_ring.one())]) == (R.one(), R.one())
        n = R.random_element(degree=19)
        d = R.random_element(degree=20)
        points = [p for p in range(100) if d(p) != 0][:41]
        pairs = [(p, n(p)/d(p)) for p in points]
        nc, dc = R.rational_interpolation(pairs)
        assert nc/dc-n/d == 0

def test_serial():
    for base_ring in [QQ, GF(2**31-1)]:
        ser = Serial()
        R = PolynomialRing(base_ring, 't')
        p = R.random_element(degree=100)
        assert ser.recons(*ser.explode(p)) == p
        assert ser.recons(*ser.explode([p, 1+p])) == [p, 1+p]
        assert ser.recons(*ser.explode((p, 1+p))) == (p, 1+p)
        assert ser.recons(*ser.explode(p/(1+p))) == p/(1+p)
        assert ser.recons(*ser.explode([])) == []
        assert ser.recons(*ser.explode(Matrix(2,[p,1,1+p,0]))) == Matrix(2,[p,1,1+p,0])

        elt = {4: Matrix(2,[p,1,1+p,0]), "toto": p}
        assert ser.recons(*ser.explode(elt)) == elt

        Dop, t, Dt = DifferentialOperators(base_ring, var='t')
        elt = Dt**3 + p*Dt**2 + (1+p)*Dt + 1
        assert ser.recons(*ser.explode(elt)) == elt


def test_FunctionReconstruction():
    R = PolynomialRing(GF(2**31-1), 't')
    denom = R.random_element(degree=40)
    mat = Matrix(4, 5, [R.random_element(degree=10)/denom for _ in range(20)])
    fr = FunctionReconstruction(R, lambda pt: mat(pt))
    cand = fr.recons()
    assert mat == cand

    cand, denom0 = fr.recons(denomapart=True)
    assert denom.leading_coefficient()*denom0 == denom and denom0*mat == cand

def test_FunctionReconstruction_matinv():
    R = PolynomialRing(GF(2**31-1), 't')
    mat = Matrix(5, 5, [R.random_element(degree=8) for _ in range(25)])
    cand = FunctionReconstruction(R, lambda pt: mat(pt).inverse()).recons()
    assert mat.inverse() == cand


def test_ModularReconstruction_matinv():
    mat = Matrix(5, 5, [QQ.random_element(1000) for _ in range(25)])
    cand = ModularReconstruction(lambda prime: mat.change_ring(GF(prime)).inverse()).recons()
    assert mat.inverse() == cand


# def test_ModularFunctionReconstruction_matinv():
#     logging.getLogger('numperiods.interpolation').setLevel(logging.INFO)
#     R = PolynomialRing(QQ, 't')
#     mat = Matrix(5, 5, [R.random_element(degree=8) for _ in range(25)])

#     def ev(prime, pt):
#         return ( mat(pt).change_ring(GF(prime)).inverse(), None )

#     cand = ModularFunctionReconstruction(R, ev).recons()

#     assert mat.inverse() == cand


