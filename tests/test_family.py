import pytest

from numperiods import config
from numperiods.cohomology import *
from numperiods.family import *
from sage.rings.all import *

import ore_algebra

def test_picard_fuchs():
    R = PolynomialRing(QQ, 'w,x,y,z')
    w,x,y,z = R.gens()
    S = PolynomialRing(R, 't')
    t = S.gen()

    f = x**4+y**4+z**4+w**4
    g = f + x*y*z*w + x*y**2*w
    lf = LinearFamily(f, g)

    genpt = QQ(1235)/11
    cydecg = lf.generators_of_cyclic_decomposition()
    coh = Cohomology(lf.pol(genpt))
    for v in cydecg:
        deq = lf.picard_fuchs_equation(v).coefficients()
        der = lf.pol.derivative()(genpt)
        b = v * vector(lf.basis)
        test = sum(deq[i](genpt)*b*(-der)**i for i in range(len(deq)))
        assert coh.red(test) == 0

def test_picard_fuchs2():
    R = PolynomialRing(QQ, 'w,x,y,z')
    w,x,y,z = R.gens()
    S = PolynomialRing(R, 't')
    t = S.gen()

    f = x**4+y**4+z**4+w**4
    g = f + x*y*z*w + x*y**2*w
    lf = LinearFamily(g, f)

    genpt = QQ(1235)/11
    cydecg = lf.generators_of_cyclic_decomposition()
    coh = Cohomology(lf.pol(genpt))
    for v in cydecg:
        deq = lf.picard_fuchs_equation(v).coefficients()
        der = lf.pol.derivative()(genpt)
        b = v * vector(lf.basis)
        test = sum(deq[i](genpt)*b*(-der)**i for i in range(len(deq)))
        assert coh.red(test) == 0


def test_numerical_transition():
    config.precision = 200
    R = PolynomialRing(QQ, 'x,y,z,w')
    x,y,z,w = R.gens()
    S = PolynomialRing(R, 't')
    t = S.gen()

    f = x**4+y**4+z**4+w**4
    g = f + x*y*z*w + x*y**2*w  # + y**3*w #+ y*z**2*w
    F = (1-t)*g+t*f
    lf0 = Family((1-t)*f + t*g)
    lf1 = Family((1-t)*g + t*f)

    tmat0 = lf0.numerical_transition_matrix()
    tmat1 = lf1.numerical_transition_matrix()

    assert (tmat1*tmat0 - Matrix.identity(21)).norm()*1000000 < 1


def test_numerical_transition2():
    config.precision = 200
    R = PolynomialRing(QQ, 'x,y,z,w')
    x,y,z,w = R.gens()
    S = PolynomialRing(R, 't')
    t = S.gen()

    f = x**4+y**4+z**4+w**4
    g = f + x*y*z*w  # + y**3*w #+ y*z**2*w
    h = g + x*y**2*w

    lf0 = LinearFamily(f, g)
    lf1 = LinearFamily(g, h)
    lf2 = LinearFamily(h, f)

    tmat0 = lf0.numerical_transition_matrix()
    tmat1 = lf1.numerical_transition_matrix()
    tmat2 = lf2.numerical_transition_matrix()

    assert (tmat2*tmat1*tmat0 - Matrix.identity(21)).norm()*1000000 < 1

