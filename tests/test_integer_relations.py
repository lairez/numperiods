import pytest

from numperiods import integer_relations
from sage.matrix.constructor import Matrix
from sage.rings.all import *


def test_random_lattice():
    m = 20
    r = 10
    lat = Matrix.random(ZZ, r, m)
    RF = RealField(2000)
    CF = ComplexIntervalField(2000)
    mix = lat.right_kernel_matrix().transpose() * Matrix.random(CF, m-r, m)
    recons = integer_relations.IntegerRelations(mix)
    assert lat.row_module() == recons.basis.row_module()

