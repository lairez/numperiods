import pytest

from numperiods.cohomology import *
from sage.rings.all import *

@pytest.fixture(params=[dict(degree=5,nvars=3), dict(degree=4,nvars=4)])
def polynomial(request):
    R = PolynomialRing(QQ, request.param['nvars'], 'x')
    f = R.random_element(terms=100, degree=request.param['degree']).homogenize(R.gen(0))
    return f

def test_gdred(polynomial):
    c = Cohomology(polynomial)
    R = polynomial.parent()

    shift = - R.ngens() % polynomial.degree()
    q = (shift+R.ngens()) / polynomial.degree()
    pol = R.gen(0)**shift

    assert c.red(polynomial*pol) == c.red(pol)
    assert c.red(polynomial*polynomial*pol) == 2*c.red(pol)
    # This is a bit slow
    #assert c.red(polynomial**3*pol) == 6*c.red(pol)

def test_basis():
    R = PolynomialRing(QQ, 4, 'w,x,y,z')
    w,x,y,z = R.gens()
    f = x**4+y**4+z**4+w**4
    co = Cohomology(f)
    assert co.basis() == [R.one(), y**2*z**2, x*y*z**2, w*y*z**2, x**2*z**2,
                          w*x*z**2, w**2*z**2, x*y**2*z, w*y**2*z, x**2*y*z,
                          w*x*y*z, w**2*y*z, w*x**2*z, w**2*x*z, x**2*y**2,
                          w*x*y**2, w**2*y**2, w*x**2*y, w**2*x*y, w**2*x**2,
                          w**2*x**2*y**2*z**2]
