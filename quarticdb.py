#!/usr/bin/env python2.7

import sys
import os
import lmdb
import argparse
import re
from datetime import datetime
import logging
import signal
import random
import platform
import json
import contextlib

import sage.all
from sage.functions.other import floor
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.rational_field import QQ
from sage.misc.persist import dumps, loads
from sage.rings.complex_arb import ComplexBallField
from sage.rings.complex_interval_field import ComplexIntervalField

import numperiods

# CONFIGURATION VARIABLES
MAPSIZE = 2**36
DATABASEPATH = './data/db'
EXTRADATAPATH = './data/extra'
LOGPATH = './log'
QDB = None

# LOGGING CONFIGURATION
log_format = "%(asctime)s:pid"+str(os.getpid())+":%(levelname)s:%(name)s:%(message)s"
log_formatter = logging.Formatter(log_format)
root_logger = logging.getLogger()
logger = logging.getLogger('quarticdb')
std_handler = logging.StreamHandler()
std_handler.setFormatter(log_formatter)
logger.addHandler(std_handler)

for name in ['quarticdb', 'numperiods.periods', 'numperiods.family',
             'numperiods.integer_relations', 'ore_algebra.analytic.analytic_continuation']:
    logging.getLogger(name).setLevel(logging.INFO)

ring = PolynomialRing(QQ, 'w,x,y,z')


class HierarchicalCursor(object):
    """
    Handles a LMDB cursor in a database where keys are absolute paths like
    `'/this/is/a/path'` representing files.

    `qdb`:
        a `Quarticdb` instance.

    `cursor`:
        underlying cursor, may be share by several instances.

    `path`:
        position of the cursor.
    """
    def __init__(self, qdb, cursor, path):
        # Holding a pointer to qdb is useful for retrieving the path to extra data.
        self.qdb = qdb
        self.cursor = cursor
        self.path = path.rstrip('/')

    def __repr__(self):
        return self.path.__repr__()

    def iter(self):
        """
        Return an iterator that yields new cursor for every child node for the
        current cursor. All instances share the same underlying LMDB cursor, which can
        be moved anywhere without disturbing the iterator.
        """

        # This regexp matches the absolute path of the child nodes.
        prog = re.compile('(%s/[^/]+)($|/)' % re.escape(self.path))
        self.cursor.set_range(self.path)
        key = str(self.cursor.key())
        match = prog.match(key)
        while match:
            yield HierarchicalCursor(self.qdb, self.cursor, match.group(0))
            if match.group(2) == '/':
                # The child node is a (sort of) directory. We jump over all subnodes.
                #
                # In lexicographic order, "xxx/yyy" < "xxx0", whatever is yyy
                # because '0' is the next character after '/' in ASCII.
                eodb = self.cursor.set_range(match.group(1) + '0') #
            else:
                # The child node is (sort of) file. We go to the next node.
                # It is not safe to use "next()" here because the cursor may have moved
                eodb = self.cursor.set_range(key + '\001')

            if not eodb:
                return

            key = str(self.cursor.key())
            match = prog.match(key)

    def ls(self):
        """Return the list of the keys associated to all child nodes.

        """
        return [c.path for c in self.iter()]


    def __getitem__(self, key):
        """Return a HierarchicalCursor instance for `os.path.join(self.path, key)`
        that shares the same underlying cursor as self. Does not actually move
        the cursor nor check that the new path exists in a valid path in the
        database.
        """
        return HierarchicalCursor(self.qdb, self.cursor, os.path.join(self.path, key))

    def setitem(self, key, value, outofdb=False, pickle=True):
        """
        Write `value` in the database with key `os.path.join(self.path, key)`.

        `outofdb`:
            If `True`, write `'@'` instead of `value` and writes `values` in an external file.

        `pickle`:
            If `False`, the value is written without pickling (so it must be a string or a buffer).
        """
        if pickle:
            value = dumps(value)

        path = os.path.join(self.path, key).rstrip('/')

        if outofdb:
            logger.debug("Writing out of DB %s." % path)

            # Don't use os.path.join here because `path` is absolute
            fullpath = self.qdb.extradata_path + path + '.sobj'
            dirpath = os.path.dirname(fullpath)
            if not os.path.isdir(dirpath):
                os.makedirs(dirpath)
            with open(fullpath, 'wb') as f:
                f.write(value)

            ret = self.cursor.put(path, '@')
        else:
            logger.debug("Writing %s." % path)
            ret = self.cursor.put(path, value)

        if not ret:
            raise lmdb.Error()

    def value(self, unpickle=True):
        """Return the value associated with the key `self.path` in the database.
        Handle as expected values stored externally.

        `unpickle`:
            If `False`, do not unpickle the return value.
        """
        if self.cursor.set_key(self.path.encode()):
            value = self.cursor.value()
            if value == '@':
                # Don't use os.path.join here because self.path starts with a '/'.
                with open(self.qdb.extradata_path + self.path + '.sobj', 'rb') as f:
                    value = f.read()

            if unpickle:
                return loads(value)
            else:
                return value
        else:
            return None

    def __setitem__(self, s, value):
        self.setitem(s, value)

    def delete(self):
        self.cursor.set_range(self.path)
        key = str(self.cursor.key())
        if key == self.path:
            self.cursor.delete()
            return

        prog = re.compile('%s/' % re.escape(self.path))
        match = prog.match(key)
        while match:
            if self.cursor.value() == '@':
                path = self.qdb.extradata_path + key + '.sobj'
                os.remove(path)
                try:
                    os.removedirs(os.path.dirname(path))
                except OSError:
                    pass

            self.cursor.delete()
            key = str(self.cursor.key())
            match = prog.match(key)

    def __contains__(self, elt):
        """Return `True` if ``elt`` is a child node of ``self``. """

        path = os.path.join(self.path, elt).rstrip('/')
        self.cursor.set_range(path)
        k = str(self.cursor.key())
        return k == path or k.startswith(path + '/')

    def putd(self, d):
        """Write a dictionary to the database.
        """
        if isinstance(d, dict):
            for k, v in d.items():
                if k[-1] == '/':
                    self[k[:-1]].putd(v)
                elif k[-1] == '@':
                    self.setitem(k[:-1], v, outofdb=True)
                else:
                    self[k] = v
        else:
            self.putd(d._save_as())


class Quarticdb(object):
    """A wrapper around a LMDB.Environment."""
    def __init__(self, rootpath):
        self.rootpath = rootpath
        self.database_path = os.path.join(rootpath, DATABASEPATH)
        self.extradata_path = os.path.join(rootpath, EXTRADATAPATH)
        self.env = lmdb.open(self.database_path, map_size=MAPSIZE)
        self.putqueue = []

    @contextlib.contextmanager
    def cursor(self, path, **kwargs):
        """Opens a transaction. To be use in a context manager `with`.
        Extra keywords arugments are passed to `self.env.begin` (see `lmdb` documentation).

        Example:

        > with qdb.cursor('/quartic') as c:
        >     # `c` is a hierarchical cursor.
        >     print(c.ls())
        """
        with self.env.begin(**kwargs) as txn:
            yield HierarchicalCursor(self, txn.cursor(), path)

    def __getitem__(self, key):
        with self.cursor('') as c:
            v = c[key].value()
        return v

    def __setitem__(self, key, value):
        with self.cursor('', write=True) as c:
            c[key] = value

    def putd(self, key, value):
        with self.cursor(key, write=True) as c:
            c.putd(value)

    def delayedput(self, key, value, pickle=True):
        if pickle:
            value = dumps(value)
        self.putqueue.append( (key, value) )

        if len(self.putqueue) > 100:
            self.flushqueue()

    def delayedputd(self, path, d):
        """Write a dictionary to the database.
        """
        if isinstance(d, dict):
            for k, v in d.items():
                if k[-1] == '/':
                    self.delayedputd(os.path.join(path,k[:-1]), v)
                elif k[-1] == '@':
                    raise NotImplementedError()
                else:
                    self.delayedput(os.path.join(path,k), v)
        else:
            self.delayedputd(path, d._save_as())


    def flushqueue(self):
        logger.info("Flushing write queue.")
        with self.cursor('', write=True) as c:
            for k, v in self.putqueue:
                c.setitem(k, v, pickle=False)
        self.putqueue = []

class ComputationContext(object):
    def __init__(self, name, *args, **kwargs):
        self.name = name
        self.args = args
        self.kwargs = kwargs
        self.id = "{name}-{timestamp}-pid{pid!s}".format(
            name=self.name,
            timestamp=datetime.now().isoformat(),
            pid=os.getpid())

    @contextlib.contextmanager
    def begin(self, log=True):
        if log:
            log_file = os.path.join(LOGPATH, self.id + ".log")
            log_handler = logging.FileHandler(log_file)
            log_handler.setFormatter(log_formatter)
            root_logger.addHandler(log_handler)

        try:
            logger.info("Starting computation {}.".format(self.id))
            logger.info("ARGS= %s, %s" % (self.args, self.kwargs))
            yield self
            if log:
                os.remove(log_file)
        except KeyboardInterrupt as e:
            logger.exception(str(e))
            sys.exit(1)
        except:
            logger.exception("Uncaught exception.")
        finally:
            if log:
                log_handler.close()
                root_logger.removeHandler(log_handler)


class PeriodComputationByDeformation(object):
    def __init__(self, qdb, args):
        self.qdb = qdb
        self.args = args

    def run(self, target, reference):
        if not target.is_homogeneous() or not target.jacobian_ideal().dimension() == 0:
            raise numperiods.cohomology.NotSmoothError()

        with self.qdb.cursor('/quartic') as c:
            if 'primitive_periods' in c[numperiods.Hypersurface(target).keyid()]:
                logger.info("Periods are already known, skipping computation.")
                return

            reference_pm = c[numperiods.Hypersurface(reference).keyid()]['primitive_periods'].value()
            if reference_pm is None:
                raise Exception("Could not load reference primitive periods.")

        prim = None
        try:
            prim = reference_pm.deformation(target)
        except numperiods.FailFast as e:
            logger.info(str(e))
        else:
            with self.qdb.cursor('/quartic', write=True) as c:
                logger.info("Saving primitive periods.")
                c.putd(prim.hypersurface)
                c.putd(prim)
                c.putd(prim.simple_periods())
            return True

        logger.info("Trying to compute period vector only.")
        simple = None
        try:
            simple = reference_pm.deformation(target, only_simple_periods=True)
        except numperiods.FailFast as e:
            logger.info(str(e))
        else:
            with self.qdb.cursor('/quartic', write=True) as c:
                logger.info("Saving simple periods.")
                c.putd(simple.hypersurface)
                c.putd(simple)
            return

        return

    def main(self):
        monomials = (sum(ring.gens())**4).monomials()
        numperiods.config.max_dimension_of_a_cyclic_space = self.args.max_deq_order-1

        cnt = 0
        while True:
            if cnt % 20 == 0:
                quartics = []
                with QDB.cursor('/quartic') as c:
                    for q in c.iter():
                        if 'primitive_periods' in q:
                            if (args.only_pic == 0 or
                                q['picard/rank'].value() == self.args.only_pic):
                                quartics.append(q['polynomial'].value())

                logger.info("Pool of %d quartics." % len(quartics))

            reference = random.choice(quartics)
            monomial = random.choice(monomials)
            sign = random.choice([-1,1])
            target = reference + sign*monomial

            with ComputationContext(self.__class__.__name__).begin():
                self.run(target, reference)

            cnt += 1


def cmd_periods(args):
    PeriodComputationByDeformation(QDB, args).main()


class GlobalIterator(object):
    """A class to iterate over a pseudodirectory in a parallel way."""

    def __init__(self, qdb, path, **kwargs):
        self.qdb = qdb
        self.env = qdb.env
        self.path = path
        self.kwargs = kwargs
        self.dbkey = '/cursor/' + self.__class__.__name__ + path
        # Matches all child nodes
        self.prog = re.compile('(%s/[^/]*)($|/)' % re.escape(self.path))

    def __iter__(self):
        return self

    def is_todo(self, hcursor):
        raise NotImplementedError

    def run(self, key):
        raise NotImplementedError

    def launch(self, log=True):
        while True:
            with self.env.begin(write=True) as txn:
                c = txn.cursor()
                curnext = c.get(self.dbkey, default=self.path)

                while True:
                    noteodb = c.set_range(curnext)
                    if not noteodb:
                        return

                    key = str(c.key())
                    match = self.prog.match(key)
                    if not match:
                        return

                    # cf. comment in HierarchicalCursor.iter()
                    if match.group(2) == '/':
                        curnext = match.group(1) + '0'
                    else:
                        curnext = match.group(1) + '\001'

                    if self.is_todo(HierarchicalCursor(self.qdb, c, match.group(1))):
                        c.put(self.dbkey, curnext)
                        break

            with ComputationContext(self.__class__.__name__, match.group(1)).begin(log=log):
                self.run(match.group(1))


class Dispatcher(object):
    def __init__(self, qdb, path, clargs, **kwargs):
        self.qdb = qdb
        self.env = qdb.env
        self.path = path
        self.kwargs = kwargs
        self.totaljobs = clargs.nb_jobs
        self.jobid = clargs.job_id
        self.args = clargs

    def run(self, hcursor):
        raise NotImplementedError

    def is_todo(self, hcursor):
        raise NotImplementedError

    def launch(self, log=False):
        with self.qdb.cursor(self.path) as c:
            for q in c.iter():
                if self.jobid > 0 and (int(q.path[-16:], 16) + self.jobid) % self.totaljobs != 0:
                    continue

                if not self.is_todo(q):
                    continue

                with ComputationContext(self.__class__.__name__, q.path).begin(log=log):
                    self.run(q)

        self.qdb.flushqueue()

def cmd_seed(args):
    with ComputationContext('seed').begin():
        f = numperiods.FermatHypersurface(ring, 4).primitive_periods()

        with QDB.cursor('/quartic', write=True) as c:
            c.putd(f.hypersurface)
            c.putd(f)
            c.putd(f.simple_periods())

        if args.alt:
            from sage.symbolic.all import I
            from sage.matrix.constructor import Matrix
            chmats = [
                Matrix([[I**(i*j) for j in range(4)] for i in range(4)]),
                #Matrix([[1,1,1,1],[1,-1,-1,-1],[1,1,-1,-1],[1,-1,1,-1]])
            ]
            for chmat in chmats:
                chf = f.change_of_variables(chmat)
                with QDB.cursor('/quartic', write=True) as c:
                    c.putd(chf.hypersurface)
                    c.putd(chf)
                    c.putd(chf.simple_periods())


class PicardLatticeComputation(Dispatcher):
    def is_todo(self, hcursor):
        return self.args.force or (('picard/lattice' not in hcursor or
                                       'picard/rank' not in hcursor or
                                       'picard/half_certificate' not in
                                       hcursor) and 'simple_periods' in
                                      hcursor)

    def run(self, hcursor):
        per = hcursor['simple_periods'].value()

        pic = per.picard()
        self.qdb.delayedputd('/quartic', pic)


def cmd_picard(args):
    PicardLatticeComputation(QDB, '/quartic', args).launch()


class TranscendentalLatticeComputation(GlobalIterator):
    def is_todo(self, hcursor):
        return ('transcendental/lattice' not in hcursor or 'transcendental/endoring_dim' not in hcursor) and 'simple_periods' in hcursor

    def run(self, key):
        with self.qdb.cursor(key) as c:
            per = c['simple_periods'].value()

        tr = per.transcendental()
        self.qdb.putd(self.path, tr)


def cmd_transcendental(args):
    TranscendentalLatticeComputation(QDB, '/quartic').launch()


class CurveCountComputation(GlobalIterator):
    def is_todo(self, hcursor):
        return (('p1count' not in hcursor
                 or self.kwargs['max_deg'] not in hcursor['p1count'].value())
                and 'picard/lattice' in hcursor)

    def run(self, key):
        with self.qdb.cursor(key) as c:
            pic = c['picard/lattice'].value()

        pic.precompute_smooth_rational_curves(self.kwargs['max_deg'])
        self.qdb[key + '/p1count'] = {k: len(pic.smooth_rational_curves(k))
                                      for k in range(1, self.kwargs['max_deg']+1)}


def cmd_curvecount(args):
    CurveCountComputation(QDB, '/quartic', max_deg=args.max_deg).launch()



class PicardLatticeInvariantsComputation(Dispatcher):
    def is_todo(self, hcursor):
        return ('picard/invariants' not in hcursor and 'picard/lattice' in hcursor)

    def run(self, hcursor):
        pic = hcursor['picard/lattice'].value()

        from sage.modules.free_quadratic_module_integer_symmetric import IntegralLattice
        inv = IntegralLattice(pic.intersection_matrix).discriminant_group().invariants()

        self.qdb.delayedput(hcursor.path + '/picard/invariants', [int(c) for c in inv])


def cmd_picardinv(args):
    PicardLatticeInvariantsComputation(QDB, '/quartic', args).launch()


def cmd_export(args):
    ret = []
    counter = 0
    with QDB.cursor('/quartic') as c:
        for q in c.iter():
            if args.job_id > 0 and (int(q.path[-10:], 16) + args.job_id) % args.nb_jobs != 0:
                continue

            counter += 1
            if counter > 100:
                logger.info(q.path)
                counter = 0

            try:
                row = {}
                #row['id'] = d.path
                row['pol'] = q['polynomial.str'].value()
                row['picrk'] = int(q['picard/rank'].value())
                row['disc'] = int(q['picard/lattice'].value().intersection_matrix.determinant())
                row['endoringdim'] = int(q['transcendental/endoring_dim'].value())
                row['totreal'] = bool(q['transcendental/is_totally_real'].value())
                row['p1count'] = q['p1count'].value()
                row['picdisc'] = q['picard/invariants'].value()
                row['halfcertificate'] = q['picard/half_certificate'].value()
                # if p1count:
                #     row['#lines'] = int(p1count[1])
                #     row['#quadrics'] = int(p1count[2])
                #     row['#cubics'] = int(p1count[3])

                ret.append(row)
            except:
                logger.exception("Uncaught exception.")

    if args.job_id > 0:
        args.path = args.path + '.' + str(args.job_id)
    with open(args.path, 'w') as f:
        json.dump(ret, f)


def cmd_merge_in(args):
    source = lmdb.open(args.sourcedb, subdir=os.path.isdir(args.sourcedb), map_size=MAPSIZE)

    cnt = 0
    total = 0
    with source.begin(buffers=True) as tsource:
        csource = tsource.cursor()
        with QDB.env.begin(write=True) as tdest:
            cdest = tdest.cursor()

            csource.first()
            for k, v in csource.iternext():
                total += 1
                if cdest.set_key(k)is False:
                    cdest.put(k, v)
                    cnt += 1

    logger.info("Copied %d / %d entries." % (cnt, total))


def cmd_prunecursors(args):
    with QDB.cursor('/cursor', write=True) as c:
        c.delete()



# ARGPARSE
parser = argparse.ArgumentParser(prog='quarticdb')
#parser.add_argument('--keep-logs', action='store_true')
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--job-id', default=0, type=int)
parser.add_argument('--nb-jobs', default=0, type=int)
parser.add_argument('--root', type=str, default='.')
parser.add_argument('--force', action='store_true')
subparsers = parser.add_subparsers()

parser_seed = subparsers.add_parser('seed')
parser_seed.set_defaults(cmd=cmd_seed)
parser_seed.add_argument('--alt', action='store_true')

parser_periods = subparsers.add_parser('periods')
parser_periods.add_argument('--only-pic', type=int, default=0)
parser_periods.add_argument('--max-deq-order', type=int, default=12)
parser_periods.set_defaults(cmd=cmd_periods)

parser_pic = subparsers.add_parser('picard')
parser_pic.set_defaults(cmd=cmd_picard)

parser_tr = subparsers.add_parser('transcendental')
parser_tr.set_defaults(cmd=cmd_transcendental)

parser_cc = subparsers.add_parser('curvecount')
parser_cc.set_defaults(cmd=cmd_curvecount)
parser_cc.add_argument('--max-deg', type=int, default=3)

parser_picardinv = subparsers.add_parser('picardinv')
parser_picardinv.set_defaults(cmd=cmd_picardinv)

parser_merge_in = subparsers.add_parser('merge-in')
parser_merge_in.add_argument('sourcedb')
parser_merge_in.set_defaults(cmd=cmd_merge_in)

parser_export = subparsers.add_parser('export')
parser_export.add_argument('path')
parser_export.set_defaults(cmd=cmd_export)

parser_prunecursors = subparsers.add_parser('prunecursors')
parser_prunecursors.set_defaults(cmd=cmd_prunecursors)


def fail_fast_handler(signum, frame):
    raise numperiods.FailFast()

# SIGALARM is used to kill computations that take too long.
signal.signal(signal.SIGALRM, fail_fast_handler)


if __name__ == '__main__':
    args = parser.parse_args()
    if args.verbose:
        logger.setLevel(logging.DEBUG)

    numperiods.config.fail_fast = True


    if not os.path.isdir(os.path.join(args.root, LOGPATH)):
        os.mkdir(os.path.join(args.root, LOGPATH))

    QDB = Quarticdb(args.root)
    args.cmd(args)

